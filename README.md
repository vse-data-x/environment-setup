# Environment Setup

1. Make sure you have installed Anaconda in order to use `conda` command
2. Open a terminal and execute:   
 `conda create --name NAME_OF_YOUR_ENVIRONMENT python=3.7`

3. Run `conda activate mba-env`
4. Create the requirements file to install our dependencies by executing `cat > requirements.txt` (for linux or Mac Users otherwise just create the empty file using your preferred method) :  
In the requirements file type in the following packages: 

- ipykernel
- numpy
- matplotlib
- pandas
- scikit-learn
- category_encoders
- python-time

Your command might look like this: 

![reqs](create-reqs.png)

5. Once you have added the required packages exit the file (CTRL+D for Linux or Mac Users)
6. Make sure to have added the packages by displaying the file contents using the `cat` command
7. Execute `pip install -r requirements.txt`
8. The installation should start 
9. Once it has completed the packages installation, it will display an overview of the packages and versions installed:  
![packages](installed-reqs.png)


## Recap

If you need to replicate the steps and have already created your `requirements.txt` file, simply run:  

```
conda create --name mba-env python=3.7
pip install -r requirements.txt

```
